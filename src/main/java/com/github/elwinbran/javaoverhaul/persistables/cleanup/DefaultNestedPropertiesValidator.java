/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.persistables.cleanup;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.containers.Container;
import com.github.elwinbran.javaoverhaul.containers.ContainerFactory;
import com.github.elwinbran.javaoverhaul.uei.ExceptionInformation;
import com.github.elwinbran.javaoverhaul.exceptions.SafeReturn;
import java.util.function.Function;
import com.github.elwinbran.javaoverhaul.peo.LegacySerializable;
import com.github.elwinbran.javaoverhaul.persistables.ConstrainedProperties;
import com.github.elwinbran.javaoverhaul.persistables.NestedPropertiesValidator;

public class DefaultNestedPropertiesValidator implements NestedPropertiesValidator
{

    private final ContainerFactory containerMaker;
    private final NestedPropertiesValidator constraintValidator;
    private final LegacySerializable metaConstraints;
    private final Function<LegacySerializable, SafeReturn<ConstrainedProperties>>
            constraintCreator;
    
    public DefaultNestedPropertiesValidator(ContainerFactory containerMaker, 
            NestedPropertiesValidator constraintValidator,
            LegacySerializable metaConstraints,
            Function<LegacySerializable, SafeReturn<ConstrainedProperties>>
            constraintCreator)
    {
        this.containerMaker = containerMaker;
        this.constraintValidator = constraintValidator;
        this.metaConstraints = metaConstraints;
        this.constraintCreator = constraintCreator;
    }
    
    @Override
    public Container<ExceptionInformation> validate(
            LegacySerializable constraints, LegacySerializable nestedValues)
    {
        SafeReturn<ConstrainedProperties> metaValidation = 
                this.constraintCreator.apply(constraints);
        NullableWrapper<ConstrainedProperties> nullableConstrains = 
                metaValidation.getReturn();
        if(!nullableConstrains.isNil())
        {
            ConstrainedProperties constrains = nullableConstrains.getWrapped();
            constrains.validate(nestedValues);
            // actual validation
            return this.containerMaker.emptyContainer();
        }
        return metaValidation;
    }

    @Override
    public Class<?> getIntendedClass()
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public String getIntendedClassName()
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }
    
}
