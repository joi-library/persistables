/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.persistables.cleanup;

import com.github.elwinbran.javaoverhaul.peo.JavaPrimitive;
import com.github.elwinbran.javaoverhaul.peo.Sequence;
import com.github.elwinbran.tpo.constraints.PrimitiveConstraints;
import com.github.elwinbran.tpo.constraints.PrimitiveWrapper;
import com.github.elwinbran.javaoverhaul.base.Pair;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;
import com.github.elwinbran.javaoverhaul.peo.LegacySerializable;
import com.github.elwinbran.javaoverhaul.persistables.ConstrainableProperty;
import com.github.elwinbran.javaoverhaul.persistables.ConstrainedProperties;

/**
 * Maybe some base to remove a single property??
 * 
 * 
 * @author Elwin Slokker
 * @param <ProducedT>
 * @param <ValueBoundT>
 */
public abstract class BaseConstrainedPropertiesCreator<ProducedT, ValueBoundT> 
        implements Function<LegacySerializable<?>, ConstrainedProperties<LegacySerializable>>
{

    @Override
    public ConstrainedProperties<LegacySerializable> 
        apply(LegacySerializable<?> properties)
    {
        String className = null;
        Class<?> classReference = null;
        for(Pair<String, JavaPrimitive> pair : 
                properties.getPrimitiveSinglePairs())
        {
            if(pair.getKey().equals(ConstrainedProperties.CLASS_KEY))
            {
                className = (String) pair.getValue().getValue();
                try 
                {
                    classReference = Class.forName(className);
                } 
                catch (ClassNotFoundException ex) 
                {
                    //
                }
                break;
            }
        }
        Collection<Pair<String, ConstrainableProperty<JavaPrimitive>>> 
                atomicPairs = new ArrayList<>();
        Collection<Pair<String, ConstrainableProperty<Sequence<JavaPrimitive>>>>
                sequencePairs = new ArrayList<>();
        Iterable<Pair<String, ConstrainedProperties>> compositePairs;
        Iterable<Pair<String, ConstrainableProperty<Sequence<LegacySerializable>>>>
                compositeSequencePairs;
        //get all the nested objects that stem directly from the root.
        for(Pair<String, ? extends LegacySerializable> pair : properties.getSingleComposedPairs())
        {
            boolean isFound = false;
            //check primitive keys for markers.
            for(Pair<String, JavaPrimitive> primitiveProperty :
                    (Iterable<Pair<String, JavaPrimitive>>)
                    pair.getValue().primitiveSinglePairs())
            {
                if(primitiveProperty.getKey().equals(ConstrainedProperties.TYPE_KEY))
                {
                    final String bub = (String)primitiveProperty.getValue().getValue();
                    if(bub.equals(PropertyConstraintType.SINGLE_PRIMITIVE.name()))
                    {
                        ConstrainableProperty<JavaPrimitive> possibleConstraint =
                                translateSinglePrimitive(pair.getValue());
                        if(possibleConstraint != null)
                        {
                            atomicPairs.add(new Pair<>(pair.getKey(), 
                                            possibleConstraint));
                            isFound = true;
                        }
                        else
                        {
                            //creation error?
                        }
                        break;
                    }
                    if(bub.equals(PropertyConstraintType.PRIMITIVE_SEQUENCE.name()))
                    {
                        ConstrainableProperty<Sequence<JavaPrimitive>> possibleConstraint =
                                translatePrimitiveSequence(pair.getValue());
                        if(possibleConstraint != null)
                        {
                            sequencePairs.add(new Pair<>(pair.getKey(), 
                                            possibleConstraint));
                            isFound = true;
                        }
                        else
                        {
                            //creation error?
                        }
                        break;
                    }
                    if(bub.equals(PropertyConstraintType.SINGLE_COMPOSITE.name()))
                    {
                        
                    }
                    if(bub.equals(PropertyConstraintType.COMPOSITE_SEQUENCE.name()))
                    {
                        
                    }
                }
            }
            if(!isFound)
            {
                //execute composite creator anyway.
            }
        }
        throw new UnsupportedOperationException("This is a null-implementation.");
    }
    
    public ConstrainableProperty<JavaPrimitive>
        translateSinglePrimitive(LegacySerializable properties)
    {
        for(Pair<String, JavaPrimitive> primitiveProperty :
                    (Iterable<Pair<String, JavaPrimitive>>)
                    properties.singleComposedPairs())
        {
            if(primitiveProperty.getKey().equals(ConstrainableProperty.getSinglePrimitiveConstraintTypeKey()))
            {
                PrimitiveConstraints primitiveConstraintEnum = 
                    PrimitiveConstraints.valueOf(
                            (String)primitiveProperty.getValue().getValue());
                ConstrainableProperty primitiveConstraint = 
                    primitiveConstraintEnum.getCreator().apply(properties);
                return new PrimitiveWrapper(primitiveConstraint);
            }
        }
        return null;
    }
    
    public ConstrainableProperty<Sequence<JavaPrimitive>>
        translatePrimitiveSequence(LegacySerializable properties)
    {
        for(Pair<String, JavaPrimitive> primitiveProperty :
                    (Iterable<Pair<String, JavaPrimitive>>)
                    properties.singleComposedPairs())
        {
            if(primitiveProperty.getKey().equals(ConstrainableProperty.PRIMITIVE_SEQUENCE_CONSTRAINT_TYPE_KEY))
            {
                
            }
        }
        return null;
    }
}
