/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.persistables;

import com.github.elwinbran.javaoverhaul.peo.LegacySerializable;


/**
 * This class is used for making primitive constraint objects.
 * TODO: likely to much responsibility.
 * 
 * @author Elwin Slokker
 */
public interface NumberConstraintsFactory
{
    
    public abstract LegacySerializable makeBooleanNonConstraint();
    
    public abstract LegacySerializable makeBooleanConstraint(boolean acceptedValue);
    
    public abstract LegacySerializable makeByteRange(byte lower, byte upper);
    
    public abstract LegacySerializable makeByteSpecificValues(
            Iterable<Byte> values);
    
    public abstract LegacySerializable makeByteSpecificValues(
            byte... values);
    
    public abstract LegacySerializable makeCharRange(char upper, char lower);
    
    public abstract LegacySerializable makeCharSpecificValues(
            Iterable<Character> values);
    
    public abstract LegacySerializable makeCharSpecificValues(
            char... values);
    
    public abstract LegacySerializable makeShortRange(short upper, short lower);
    
    public abstract LegacySerializable makeShortSpecificValues(
            Iterable<Short> values);
    
    public abstract LegacySerializable makeShortSpecificValues(
            short... values);
    
    public abstract LegacySerializable makeIntRange(int upper, int lower);
    
    public abstract LegacySerializable makeIntSpecificValues(
            Iterable<Integer> values);
    
    public abstract LegacySerializable makeFloatRange(float upper, float lower);
    
    public abstract LegacySerializable makeFloatSpecificValues(
            Iterable<Float> values);
    
    public abstract LegacySerializable makeLongRange(long upper, long lower);
    
    public abstract LegacySerializable makeLongSpecificValues(
            Iterable<Long> values);
    
    public abstract LegacySerializable makeDoubleRange(double upper, double lower);
    
    public abstract LegacySerializable makeDoubleSpecificValues(
            Iterable<Double> values);
}
