/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.persistables.cleanup;

import com.github.elwinbran.javaoverhaul.peo.JavaPrimitive;

/**
 *
 * @author Elwin Slokker
 */
public class DefaultPrimitiveSerializable extends JavaPrimitive
{
    /**
     * Keeps the primitive (or reference to array)
     */
    private final Object value;
    
    /**
     * Hidden constructor that simply saves the value.
     * 
     * @param object Anything, but it should be a 'primitive'.
     */
    private DefaultPrimitiveSerializable(Object object)
    {
        this.value = object;
    }
    
    public DefaultPrimitiveSerializable(Byte number)
    {
        this((Object)number);
    }
    
    public DefaultPrimitiveSerializable(final Short number)
    {
        this((Object)number);
    }
    
    public DefaultPrimitiveSerializable(final Integer number)
    {
        this((Object)number);
    }
    
    public DefaultPrimitiveSerializable(final Float number)
    {
        this((Object)number);
    }
    
    public DefaultPrimitiveSerializable(Long number)
    {
        this((Object)number);
    }
    
    public DefaultPrimitiveSerializable(Double number)
    {
        this((Object)number);
    }
    
    public DefaultPrimitiveSerializable(final String string)
    {
        this((Object)string);
    }
    
    public DefaultPrimitiveSerializable(final Character character)
    {
        this((Object)character);
    }
    
    public DefaultPrimitiveSerializable(final Boolean statement)
    {
        this((Object)statement);
    }
    
    
    /**
     * Checks if this serial value is the same type as an other.
     * 
     * @param other another primitive value.
     * @return {@code true} when this serial value is of the same type as the 
     * given value and {@code false} otherwise.
     */
    @Override
    public boolean isSameType(final JavaPrimitive other)
    {
        return this.value.getClass().equals(other.getValue().getClass());
    }
    
    /**
     * 
     * @return Ensured to be a primitive or an array/iterable of primitives.
     */
    @Override
    public Object getValue()
    {
        return this.value;
    }
}
