/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.persistables.cleanup;

import com.github.elwinbran.javaoverhaul.containers.Container;
import com.github.elwinbran.javaoverhaul.peo.JavaPrimitive;
import com.github.elwinbran.javaoverhaul.containers.Table;
import com.github.elwinbran.javaoverhaul.peo.LegacySerializable;

/**
 * An extremely basic implementation of an object that exposes all its 
 * properties.
 * 
 * @author Elwin Slokker
 */
public class DefaultNestedProperties implements LegacySerializable
{

    private final Table<String, JavaPrimitive> primitiveSinglePairs;
            
    private final Table<String, Container<JavaPrimitive>> 
            primitiveSequencePairs;
            
    private final Table<String, LegacySerializable> singleComposedPairs;
            
    private final Table<String, Container<LegacySerializable>> 
            composedSequencePairs;
    
    /**
     * Primary Constructor.
     * 
     * @param primitiveSinglePairs the 'primitive' properties of the object.
     * @param primitiveSequencePairs the 'arrays' and 'Iterables' of the object
     * that store 'primitive' values.
     * @param singleComposedPairs the 'composed' properties of the objects.
     * @param composedSequencePairs the 'arrays' and 'Iterables' of the object
     * that store 'composed' values.
     */
    public DefaultNestedProperties(
            Table<String, JavaPrimitive> primitiveSinglePairs,
            Table<String, Container<JavaPrimitive>> primitiveSequencePairs,
            Table<String, LegacySerializable> singleComposedPairs,
            Table<String, Container<LegacySerializable>> composedSequencePairs)
    {
        this.primitiveSinglePairs = primitiveSinglePairs;
        this.primitiveSequencePairs = primitiveSequencePairs;
        this.singleComposedPairs = singleComposedPairs;
        this.composedSequencePairs = composedSequencePairs;
    }
    
    /**
     * {@inheritDoc }
     */
    @Override
    public Table<String, JavaPrimitive> primitiveSinglePairs()
    {
        return this.primitiveSinglePairs;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Table<String, Container<JavaPrimitive>> primitiveSequencePairs()
    {
        return this.primitiveSequencePairs;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Table<String, LegacySerializable> singleComposedPairs()
    {
        return this.singleComposedPairs;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Table<String, Container<LegacySerializable>> composedSequencePairs()
    {
        return this.composedSequencePairs;
    }
    
}
