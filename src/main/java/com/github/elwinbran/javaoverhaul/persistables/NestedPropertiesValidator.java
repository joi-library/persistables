/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.persistables;

import com.github.elwinbran.javaoverhaul.containers.Container;
import com.github.elwinbran.javaoverhaul.uei.ExceptionInformation;
import com.github.elwinbran.javaoverhaul.peo.LegacySerializable;

/**
 * Validates an object with exposed properties through another exposed object
 * that represent the constraints on the first.
 * 
 * nestedProperties -> Constraints/Exception
 * 
 * @author Elwin Slokker
 */
public interface NestedPropertiesValidator
{
    
    /**
     * Uses the {@code constraints} to validate the given {@code nestedValues}.
     * 
     * @param constraints an object exposing it's properties that has values
     * that represent constraints for another object.
     * @param nestedValues the property exposing object that requires validity.
     * @return An empty container of {@link ExceptionInformation exceptions} if 
     * the values are within the constraints. Otherwise the container has 
     * exceptions that describe exactly what was wrong with either of the 
     * arguments.
     */
    public abstract Container<ExceptionInformation> validate(
            LegacySerializable constraints, LegacySerializable nestedValues);
    
    
}
