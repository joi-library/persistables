/**
 * NEW DESCRIPTION
 * You need a CompositeSerializable, a validator function and a creator function.
 * 
 * -Exact mode: no extra fields allowed
 * 
 * OLD DESCRIPTION
 * 
 * Main package of the TrulyPersistableObjects library.
 * Contains all classes needed to declare objects that expose the data they are 
 * composed of. This data should only be used for serialization or cloning of 
 * objects.
 * The package also contains the {@link Creator} class that provides a way of 
 * assembling objects from data.
 * 
 * TODO info dump:
 * So an object that exposes its properties for cloning or serialization must 
 * expose all of the following things:
 * -primitive value properties
 * -primitive unsorted sequence properties
 * -primitive sorted sequence properties
 * -composite value properties
 * -composite unsorted sequence properties
 * -composite sorted sequence properties
 * 
 * Where primitives are undivideable values whatever those may be, although
 * usually bytes strings etc.
 * sequences are things all things that are iterable with a for loop. Data that 
 * can be viewed as a sequence. The (un)sorted distinction is very important 
 * because data storers need to know that a certain order is important or not.
 * Composites being objects that also expose their properties.
 * 
 * This results in a a nested or hierarchical structure that is common in the 
 * OOP world. It turns out that this structure in combination with the idea that
 * all objects divide to primitives, make these objects universally persistable.
 * 
 * --CONSTRAINTS--
 * The second issue is having universal constraints for these objects. 
 * Most of the time certainty is critical and lets not forget normally 
 * created objects always have some constraints. Besides is this also usefull 
 * for data sources that are unable to persist truly anything. They might limit
 * property names (the key) to a certain length or impose other limitations.
 * 
 * Thus it was decided that constraints need to be property exposers, just like
 * XSD or JSON Schema.
 * There are four base constraints:
 * -number based primitive constraints
 * [either a range of allowed values or a set of allowed values]
 * -string primitive constraints
 * [length range, length specific values from a set, specific allowed strings or
 * plain old regex as a constraint]
 * -sequence constraints
 * [can be limited by length, sortedness, element check, semantics or a combination]
 * -composite constraints
 * [semantics and or property requirements]
 * 
 * In addition does there exist the notion of 'non-constraints'. Sometimes 
 * any value is valid as long as the data has the property present.
 * 
 * Optional properties do sort of exist, although im not sure whether they will be truly 
 * supported or not.
 * 
 * The thing for the first three kinds of constraints... they can easily be provided by 
 * the library. So they need to be carefully put in the API. the fourtht is 
 * unclear since it could be up to the library user.
 */
package com.github.elwinbran.javaoverhaul.persistables;
