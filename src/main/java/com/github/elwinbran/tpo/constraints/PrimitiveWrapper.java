/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints;

import com.github.elwinbran.tpo.NestedProperties;
import javafx.util.Pair;
import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.PrimitiveSerializable;
import com.github.elwinbran.tpo.Sequence;

/**
 * Used for converting regular primitive constraints 
 * 
 * Acquiring the properties of this object is impossible; the wrapped constraint
 * properties are acquired instead.
 * 
 * @author Elwin Slokker
 */
public class PrimitiveWrapper implements ConstrainableProperty<PrimitiveSerializable>
{
    private final ConstrainableProperty wrapped;
    
    public PrimitiveWrapper(ConstrainableProperty primitiveConstraint)
    {
        this.wrapped = primitiveConstraint;
    }
    
    @Override
    public boolean isValidValue(PrimitiveSerializable value)
    {
        return this.wrapped.isValidValue(value.getValue());
    }
    
    @Override
    public Class<?> propertyType()
    {
        return this.wrapped.propertyType();
    }

    @Override
    public Iterable<Pair<String, PrimitiveSerializable>> getPrimitiveSinglePairs()
    {
        return this.wrapped.getPrimitiveSinglePairs();
    }

    @Override
    public Iterable<Pair<String, Sequence<PrimitiveSerializable>>> getPrimitiveSequencePairs()
    {
        return this.wrapped.getPrimitiveSequencePairs();
    }
    
    @Override
    public Iterable<Pair<String, NestedProperties>> 
        getSingleComposedPairs()
    {
       return this.wrapped.getSingleComposedPairs();
    }

    @Override
    public Iterable<Pair<String,Sequence<NestedProperties>>> 
        getComposedSequencePairs()
    {
        return this.wrapped.getComposedSequencePairs();
    }
    
}
