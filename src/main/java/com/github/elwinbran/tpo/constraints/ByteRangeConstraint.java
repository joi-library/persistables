/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints;

import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.PrimitiveSerializable;
import javafx.util.Pair;

/**
 *
 * @author Elwin Slokker
 */
public class ByteRangeConstraint extends RangeConstraint<Byte>
{
    
    /**
     * The lowest signed value of the byte value.
     */
    private final byte lowerBound;
    
    /**
     * The highest signed value of the byte value.
     */
    private final byte upperBound;
    
    public ByteRangeConstraint(byte lowerBound, byte upperBound)
    {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        super.primitivePairs.add(new Pair<>(RangeConstraint.LOWER_BOUND_KEY,
            PrimitiveSerializable.make(lowerBound)));
        super.primitivePairs.add(new Pair<>(RangeConstraint.UPPER_BOUND_KEY,
            PrimitiveSerializable.make(upperBound)));
        super.primitivePairs.add(new Pair<>(
                ConstrainableProperty.SINGLE_PRIMITIVE_CONSTRAINT_TYPE_KEY,
            PrimitiveSerializable.make(PrimitiveConstraints.BYTE_RANGE.name())));
    }
    
    @Override
    public boolean isValidValue(Byte value)
    {
        return (value >= this.lowerBound &&
                value <= this.upperBound);
    }

    @Override
    public Class<?> propertyType()
    {
        return Byte.class;
    }
}
