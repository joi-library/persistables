/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints;

import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.Sequence;

/**
 *
 * @author Elwin Slokker
 * @param <PropertyT>
 */
public class SequenceConstraintBuilder<PropertyT>
{
    private String propertyName;
    
    private int lowerBound = 0;
    
    private int upperBound;
    
    private boolean isSorted;
    
    private ConstrainableProperty<PropertyT> constraint;
    
    public static <PropertyT> SequenceConstraintBuilder<PropertyT> newBuilder()
    {
        return new SequenceConstraintBuilder<>();
    }
    
    protected SequenceConstraintBuilder()
    {}
    
    
    public void setLowerBound(final int lowerBound)
    {
        
    }
    
    public void setUpperBound(final int upperBound)
    {
        
    }
    
    public void setSortConstraint(final boolean isSorted)
    {
        
    }
    
    public void setIndividualConstraint(ConstrainableProperty<PropertyT> constraint)
    {
        
    }
    
    public ConstrainableProperty<Sequence<PropertyT>> build()
    {
        
    }
}
