/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints;

import com.github.elwinbran.tpo.PrimitiveSerializable;
import javafx.util.Pair;

/**
 *
 * @author Elwin Slokker
 */
public class StringBoundsConstraint extends RangeConstraint<String>
{
    
    /**
     * The required minimum length of the string.
     */
    private final int lowerBound;
    
    /**
     * The maximum allowed length of the string.
     */
    private final int upperBound;
    
    /**
     * 
     * @param lowerBound
     * @param upperBound 
     */
    public StringBoundsConstraint(int lowerBound, 
                                  int upperBound)
    {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        super.primitivePairs.add(new Pair<>(LOWER_BOUND_KEY, 
                                            PrimitiveSerializable.make(lowerBound)));
        super.primitivePairs.add(new Pair<>(UPPER_BOUND_KEY, 
                                            PrimitiveSerializable.make(upperBound)));
    }

    @Override
    public boolean isValidValue(String value)
    {
        return (value.length() >= this.lowerBound &&
                value.length() <= this.upperBound);
    }

    @Override
    public Class<?> propertyType()
    {
        return String.class;
    }
    
}
