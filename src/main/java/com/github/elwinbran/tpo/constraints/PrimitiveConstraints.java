/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints;

import com.github.elwinbran.tpo.constraints.creators.StringRegexConstraintCreator;
import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.NestedProperties;
import com.github.elwinbran.tpo.constraints.creators.*;
import java.util.function.Function;

/**
 *
 * @author Elwin Slokker
 */
public enum PrimitiveConstraints
{
    BOOLEAN_CONSTRAINT(BooleanConstraintCreator.getInstance()),
    
    BYTE_RANGE(ByteRangeConstraintCreator.getInstance()),
    BYTE_SPECIFIC_VALUES(ByteValuesConstraintCreator.getInstance()),
    
    SHORT_RANGE(ShortRangeConstraintCreator.getInstance()),
    SHORT_SPECIFIC_VALUES(ShortValuesConstraintCreator.getInstance()),
    
    CHAR_RANGE(CharacterRangeConstraintCreator.getInstance()),
    CHAR_SPECIFIC_VALUES(CharacterValuesConstraintCreator.getInstance()),
    
    INT_RANGE(IntegerRangeConstraintCreator.getInstance()),
    INT_SPECIFIC_VALUES(IntegerValuesConstraintCreator.getInstance()),
    
    LONG_RANGE(LongRangeConstraintCreator.getInstance()),
    LONG_SPECIFIC_VALUES(LongValuesConstraintCreator.getInstance()),
    
    FLOAT_RANGE(FloatRangeConstraintCreator.getInstance()),
    FLOAT_SPECIFIC_VALUES(FloatValuesConstraintCreator.getInstance()),
    
    DOUBLE_RANGE(DoubleRangeConstraintCreator.getInstance()),
    DOUBLE_SPECIFIC_VALUES(DoubleValuesConstraintCreator.getInstance()),
    
    STRING_LENGTH_RANGE,
    STRING_SPECIFIC_LENGTH_VALUES,
    STRING_SPECIFIC_VALUES,
    STRING_REGEX(StringRegexConstraintCreator.getInstance()),
    
    SEQUENCE_LENGTH_RANGE,
    SEQUENCE_SPECIFIC_LENGTH_VALUES,
    SEQUENCE_SORTED,
    SEQUENCE_ELEMENT_CHECK,
    SEQUENCE_COMBINATION;
    
    private final Function<NestedProperties<?>, ConstrainableProperty> creator;
    
    private PrimitiveConstraints(Function<NestedProperties<?>,
                                 ConstrainableProperty> creator)
    {
        this.creator = creator;
    }
    
    public Function<NestedProperties<?>, ConstrainableProperty> getCreator()
    {
        return this.creator;
    }
}
