/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints.creators;

import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.NestedProperties;
import com.github.elwinbran.tpo.PrimitiveSerializable;
import com.github.elwinbran.tpo.constraints.ShortRangeConstraint;
import com.github.elwinbran.tpo.constraints.RangeConstraint;
import java.util.function.Function;
import javafx.util.Pair;

/**
 *
 * @author Elwin Slokker
 */
public class ShortRangeConstraintCreator implements 
        Function<NestedProperties<?>, ConstrainableProperty>
{
    private static final Function<NestedProperties<?>, 
            ConstrainableProperty> INSTANCE = new ShortRangeConstraintCreator();
    
    public static Function<NestedProperties<?>, ConstrainableProperty>
            getInstance()
    {
        return INSTANCE;
    }
    
    private ShortRangeConstraintCreator()
    {}
          
    @Override
    public ConstrainableProperty<Short> apply(NestedProperties<?> t)
    {
        boolean isLowerFound = false;
        short lowerBound = 0;
        for(Pair<String, PrimitiveSerializable> pair : 
                t.getPrimitiveSinglePairs())
        {
            if(pair.getKey().equals(RangeConstraint.LOWER_BOUND_KEY))
            {
                lowerBound = (Short) pair.getValue().getValue();
                isLowerFound = true;
                break;
            }
        }
        if(isLowerFound)
        {
            for(Pair<String, PrimitiveSerializable> pair : 
                    t.getPrimitiveSinglePairs())
            {
                if(pair.getKey().equals(RangeConstraint.UPPER_BOUND_KEY))
                {
                    return new ShortRangeConstraint(lowerBound, 
                            (Short) pair.getValue().getValue());
                }
            }
        }
        throw new IllegalArgumentException("Provided properties did not contain"
                + " range values.");
    }
}
