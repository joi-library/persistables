/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints;

import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.PrimitiveSerializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javafx.util.Pair;

/**
 * Acts as the object that can validate every type of boolean property.
 * Contains two single primitive pairs:
 * -default constraint identifier
 * -{@link #CONSTRAINT_KEY}
 * Effectively final.
 * 
 * @author Elwin Slokker
 */
public class BooleanConstraint implements ConstrainableProperty<Boolean>
{

    /**
     * 
     */
    protected static final String CONSTRAINT_KEY = "booleanConstraint";
    
    /**
     * Internal value that signals that only false booleans are valid.
     */
    private static final byte BOOLEAN_FALSE = 0;
    
    /**
     * Internal value that signals that only true booleans are valid.
     */
    private static final byte BOOLEAN_TRUE = 1;
    
    /**
     * Internal value that signals that true or false are both valid.
     */
    private static final byte ANY_BOOLEAN = 2;
    
    public static String getConstraintKey()
    {
        return CONSTRAINT_KEY;
    }
    
    /**
     * Keeps the reference whether a boolean is considered valid of not.
     */
    private final byte booleanConstraint;
    
    private final Collection<Pair<String, PrimitiveSerializable>> primitivePairs
            = new ArrayList<>(2);
    
    public BooleanConstraint()
    {
        this(ANY_BOOLEAN);
    }
    
    public BooleanConstraint(boolean acceptedValue)
    {
        this((acceptedValue)? BOOLEAN_TRUE: BOOLEAN_FALSE);
    }
            
    public BooleanConstraint(byte booleanConstraint)
    {
        this.booleanConstraint = booleanConstraint;
        this.primitivePairs.add(new Pair<>(getConstraintKey(), 
                PrimitiveSerializable.make(this.booleanConstraint)));
        this.primitivePairs.add(new Pair<>(
                ConstrainableProperty.getSinglePrimitiveConstraintTypeKey(),
                PrimitiveSerializable.make(
                        PrimitiveConstraints.BOOLEAN_CONSTRAINT.name())));
    }
    
    @Override
    public boolean isValidValue(Boolean value)
    {
        if(this.booleanConstraint == ANY_BOOLEAN)
        {
            return true;
        }
        else if(this.booleanConstraint == BOOLEAN_FALSE)
        {
            return !value;
        }
        return value;
    }

    @Override
    public Class<?> propertyType()
    {
        return Boolean.class;
    }

    @Override
    public Iterable getPrimitiveSinglePairs()
    {
        return this.primitivePairs;
    }

    @Override
    public Iterable getPrimitiveSequencePairs()
    {
        return Collections.EMPTY_LIST;
    }

    @Override
    public Iterable getSingleComposedPairs()
    {
        return Collections.EMPTY_LIST;
    }

    @Override
    public Iterable getComposedSequencePairs()
    {
        return Collections.EMPTY_LIST;
    }
    
}
