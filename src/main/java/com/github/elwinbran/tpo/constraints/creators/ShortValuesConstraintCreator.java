/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints.creators;

import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.NestedProperties;
import com.github.elwinbran.tpo.PrimitiveSerializable;
import com.github.elwinbran.tpo.Sequence;
import com.github.elwinbran.tpo.constraints.ShortValuesConstraint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;
import javafx.util.Pair;

/**
 *
 * @author Elwin Slokker
 */
public class ShortValuesConstraintCreator implements 
        Function<NestedProperties<?>, ConstrainableProperty>
{
    private static final Function<NestedProperties<?>, 
            ConstrainableProperty> INSTANCE = new ShortValuesConstraintCreator();
    
    public static Function<NestedProperties<?>, ConstrainableProperty>
            getInstance()
    {
        return INSTANCE;
    }
    
    private ShortValuesConstraintCreator()
    {}
    
    @Override
    public ConstrainableProperty apply(NestedProperties<?> t)
    {
        for(Pair<String, Sequence<PrimitiveSerializable>> pair : 
                t.getPrimitiveSequencePairs())
        {
            if(pair.getKey().equals(ShortValuesConstraint.ALLOWED_VALUES_KEY))
            {
                Collection<Short> allowedValues = new ArrayList<>();
                for(PrimitiveSerializable possibleShort : pair.getValue().getSequence())
                {
                    allowedValues.add((short) possibleShort.getValue());
                }
                return new ShortValuesConstraint(allowedValues);
            }
        }
        throw new IllegalArgumentException("Provided properties did not contain"
                + "  a set of allowed values.");
    }
    
}
