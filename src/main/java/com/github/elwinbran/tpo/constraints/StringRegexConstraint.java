/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints;

import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.PrimitiveSerializable;
import java.util.Collections;
import javafx.util.Pair;

/**
 *
 * @author Elwin Slokker
 */
public class StringRegexConstraint implements ConstrainableProperty<String>
{
    /**
     * Meant for creators.
     */
    protected static final String REGEX_KEY = "regularExpression";
    
    /**
     * The Regular Expression this object uses to validate values for 
     * the property.
     */
    private final String regularExpression;
    
    public StringRegexConstraint(String regularExpression)
    {
        this.regularExpression = regularExpression;
        //super.primitivePairs.add(new Pair<>(REGEX_KEY , NewPrimitive.make(this.regularExpression)));
    }

    @Override
    public boolean isValidValue(String value)
    {
        return value.matches(this.regularExpression);
    }

    @Override
    public Class<?> propertyType()
    {
        return String.class;
    }

    @Override
    public Iterable<Pair<String, PrimitiveSerializable>> getPrimitiveSinglePairs()
    {
        return super.primitivePairs;
    }

    @Override
    public Iterable getSingleComposedPairs()
    {
        return Collections.EMPTY_LIST;
    }

    @Override
    public Iterable getPrimitiveSequencePairs()
    {
        return Collections.EMPTY_LIST;
    }

    @Override
    public Iterable getComposedSequencePairs()
    {
        return Collections.EMPTY_LIST;
    }
    
}
