/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints.creators;

import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.ConstrainedProperties;
import com.github.elwinbran.tpo.NestedProperties;
import com.github.elwinbran.tpo.OldPrimitive;
import javafx.util.Pair;

/**
 *
 * @author Elwin Slokker
 */
public class NullConstraints implements ConstrainedProperties
{

    @Override
    public Iterable<ConstrainableProperty<OldPrimitive>> getAtomicConstraints()
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public Iterable<ConstrainableProperty<Iterable<OldPrimitive>>> getAtomicSequenceConstraints()
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public Iterable<Pair<String, ConstrainedProperties>> getCompositeConstraints()
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public Class<?> getIntededClass()
    {
        return Object.class;
    }
    
    @Override
    public boolean isValid(NestedProperties properties)
    {
        return false;
    }
}
