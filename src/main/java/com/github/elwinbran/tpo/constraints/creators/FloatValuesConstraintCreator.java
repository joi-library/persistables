/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.constraints.creators;

import com.github.elwinbran.tpo.ConstrainableProperty;
import com.github.elwinbran.tpo.NestedProperties;
import com.github.elwinbran.tpo.PrimitiveSerializable;
import com.github.elwinbran.tpo.Sequence;
import com.github.elwinbran.tpo.constraints.FloatValuesConstraint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;
import javafx.util.Pair;

/**
 *
 * @author Elwin Slokker
 */
public class FloatValuesConstraintCreator implements 
        Function<NestedProperties<?>, ConstrainableProperty>
{
    private static final Function<NestedProperties<?>, 
            ConstrainableProperty> INSTANCE = new FloatValuesConstraintCreator();
    
    public static Function<NestedProperties<?>, ConstrainableProperty>
            getInstance()
    {
        return INSTANCE;
    }
    
    private FloatValuesConstraintCreator()
    {}
    
    @Override
    public ConstrainableProperty apply(NestedProperties<?> t)
    {
        for(Pair<String, Sequence<PrimitiveSerializable>> pair : 
                t.getPrimitiveSequencePairs())
        {
            if(pair.getKey().equals(FloatValuesConstraint.ALLOWED_VALUES_KEY))
            {
                Collection<Float> allowedValues = new ArrayList<>();
                for(PrimitiveSerializable possibleFloat : pair.getValue().getSequence())
                {
                    allowedValues.add((float) possibleFloat.getValue());
                }
                return new FloatValuesConstraint(allowedValues);
            }
        }
        throw new IllegalArgumentException("Provided properties did not contain"
                + "  a set of allowed values.");
    }
    
}