/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.tpo.example;

import com.github.elwinbran.javaoverhaul.persistables.ConstrainedProperties;
import com.github.elwinbran.javaoverhaul.exceptions.SafeReturn;
import java.util.function.Function;
import com.github.elwinbran.javaoverhaul.peo.LegacySerializable;

/**
 *
 * @author Elwin Slokker
 */
public class Example
{

    public static void main(String[] args)
    {
        //API definition
        /*
          here come a load of standard constraints and factory stuff to be able 
          to load other constraints.
        */
        Function<LegacySerializable, SafeReturn<ConstrainedProperties>> constraintCreator = null;
        LegacySerializable bubConstraints = null;
        SafeReturn<ConstrainedProperties> possibleConstraint
                = constraintCreator.apply(bubConstraints);
        if (!possibleConstraint.getReturn().isNil())
        {
            ConstrainedProperties bubSpecs = 
                    possibleConstraint.getReturn().getWrapped();
            //the bubSpecs must be used to create the bubCreator.
            Function<LegacySerializable, SafeReturn<Bub>> bubCreator = 
                    new BubCreator(bubSpecs);
            SafeReturn<Bub> possibleBub = bubCreator.apply(null);
            if (!possibleBub.getReturn().isNil())
            {
                Bub bubbus = possibleBub.getReturn().getWrapped();
            }
        }
    }

    private static class Bub
    {

    }
    
    private static class BubCreator implements 
            Function<LegacySerializable, SafeReturn<Bub>>
    {

        private final ConstrainedProperties bubConstrain;
        
        BubCreator(ConstrainedProperties bubConstrain)
        {
            this.bubConstrain = bubConstrain;
        }
        
        @Override
        public SafeReturn<Bub> apply(LegacySerializable props)
        {
            return null;
        }
    }
}
